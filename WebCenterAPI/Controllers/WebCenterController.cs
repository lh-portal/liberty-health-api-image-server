﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Web.Http;
using System.Xml;

namespace WebCenterAPI.Controllers
{

    public class WebCenterController : ApiController
    {
        [HttpGet]
        [Route("api/webcenter/getDocInfo")]
        public string SOAPDocInfo(int docID)
        {
            const string url = "http://zactdk01.dot.co.za:16200/_dav/cs/idcplg";
            const string action = "http://www.stellent.com/DocInfo/";

            XmlDocument soapEnvelopeXml = CreateSoapEnvelope("getDocInfo", docID, null);
            HttpWebRequest webRequest = CreateWebRequest(url, action);

            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

            string result;
            using (WebResponse response = webRequest.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    result = rd.ReadToEnd();
                }
            }
            return result;
        }


        [HttpGet]
        [Route("api/webcenter/getFile")]
        public string SOAPGetFile(int fileID)
        {
            const string url = "http://zactdk01.dot.co.za:16200/_dav/cs/idcplg";
            const string action = "http://www.stellent.com/GetFile/";

            XmlDocument soapEnvelopeXml = CreateSoapEnvelope("getFile", fileID, null);
            HttpWebRequest webRequest = CreateWebRequest(url, action);

            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

            string result;
            using (WebResponse response = webRequest.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    result = rd.ReadToEnd();
                }
            }
            return result;
        }

        [HttpGet]
        [Route("api/webcenter/getSearchResults")]
        public string SOAPGetSearchResults(string searchPhrase)
        {
            const string url = "http://zactdk01.dot.co.za:16200/_dav/cs/idcplg";
            const string action = "http://www.stellent.com/Search/";

            XmlDocument soapEnvelopeXml = CreateSoapEnvelope("getSearchResults", null, searchPhrase);
            HttpWebRequest webRequest = CreateWebRequest(url, action);

            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

            string result;
            using (WebResponse response = webRequest.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    result = rd.ReadToEnd();
                }
            }
            return result;
        }

        private static HttpWebRequest CreateWebRequest(string url, string action)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            webRequest.Credentials = new NetworkCredential("ricardom","Strobin5");
            return webRequest;
        }

        private static XmlDocument CreateSoapEnvelope(string reqType, int? id, string searchPhrase)
        {
            XmlDocument soapEnvelopeXml = new XmlDocument();

            switch (reqType)
            {
                case "getDocInfo":
                    {
                        soapEnvelopeXml.LoadXml(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:doc=""http://www.stellent.com/DocInfo/""><soapenv:Header/><soapenv:Body><doc:DocInfoByID><doc:dID>" + id.ToString() +"</doc:dID></doc:DocInfoByID></soapenv:Body></soapenv:Envelope>");
                        break;
                    }
                case "getFile":
                    {
                        soapEnvelopeXml.LoadXml(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:get=""http://www.stellent.com/GetFile/""><soapenv:Header/><soapenv:Body><get:GetFileByID><get:dID>" + id.ToString() + "</get:dID></get:GetFileByID></soapenv:Body></soapenv:Envelope>");
                        break;
                    }
                case "getSearchResults":
                    {
                        soapEnvelopeXml.LoadXml(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sear=""http://www.stellent.com/Search/""><soapenv:Header/><soapenv:Body><sear:QuickSearch><sear:queryText>" + searchPhrase + "</sear:queryText></sear:QuickSearch></soapenv:Body></soapenv:Envelope>");
                        break;
                    }
            }

            soapEnvelopeXml.LoadXml(@"");

            return soapEnvelopeXml;
        }

        private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }
    }
}
